import http from 'k6/http';
import { check, sleep } from 'k6';

export let options = {
  stages: [
    { duration: '30s', target: 50 },
    { duration: '30s', target: 100 },
    { duration: '30s', target: 200 },
    { duration: '30s', target: 300 },
    { duration: '30s', target: 400 },
    { duration: '30s', target: 500 },
    { duration: '30s', target: 600 },
    { duration: '30s', target: 700 },
    { duration: '30s', target: 800 },
    { duration: '30s', target: 900 },
    { duration: '30s', target: 1000 },
    { duration: '1m', target: 50 },
    { duration: '1m', target: 5 }
  ],
};


// export let options = {
//   stages: [
//     { duration: '30s', target: 5 },
//     { duration: '30s', target: 10 },
//     { duration: '30s', target: 20 },
//     { duration: '30s', target: 30 },
//     { duration: '90s', target: 50 },
//     { duration: '30s', target: 60 },
//     { duration: '30s', target: 30 },
//     { duration: '30s', target: 0 }
//   ],
// };

export default function() {
  let res = http.get('http://reactive.34.215.5.158.nip.io/reactive/delayed/customers?delay=0.5f');
  check(res, { 'status was 200': r => r.status == 200 });
  sleep(1);
}

// Flux: Good Run
// http://grafana.34.215.5.158.nip.io/d/cWYV6aCWz/spring-boot-statistics?orgId=1&from=1586751894689&to=1586752194689&var-pod=dep-flux-mysql-67c8d55c88-dqgbt
// Web Server Got Restarted
// http://grafana.34.215.5.158.nip.io/d/cWYV6aCWz/spring-boot-statistics?orgId=1&from=1586749396593&to=1586750296593&refresh=5s&var-pod=dep-web-mysql-7c54444987-9p4j9

// http://grafana.34.215.5.158.nip.io/d/cWYV6aCWz/spring-boot-statistics?orgId=1&refresh=5s&var-pod=dep-web-mysql-7b5bfccdd4-fzmkv&from=1586746394231&to=1586747294231