#!/usr/bin/env bash

kubectl apply -f ./ingress/.

LB_HOST=$(kubectl -n ingress-nginx get svc ingress-nginx -o jsonpath="{.status.loadBalancer.ingress[0].hostname}")

echo $LB_HOST

LB_IP=$(dig +short $LB_HOST| tail -n 1)

echo LB_IP