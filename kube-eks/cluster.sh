#!/usr/bin/env bash

# For Admin Policy Creation, refer:
# https://github.com/weaveworks/eksctl/issues/204

# For generating key
aws ec2 create-key-pair --key-name minion-rx-keys

# For view Key
#aws ec2 describe-key-pairs --key-name minion-rx-keys
eksctl create cluster \
--name minion-rx-demo \
--region us-west-2 \
--nodegroup-name standard-workers \
--node-type t3.medium \
--nodes 3 \
--nodes-min 1 \
--nodes-max 4 \
--ssh-access \
--ssh-public-key minion-rx-keys \
--managed


# Cluster Role Binding
kubectl create clusterrolebinding default-admin --clusterrole cluster-admin --serviceaccount=default:default

kubectl apply -f ./kube-dashboard.yaml

# Get Token
token=$(kubectl get secrets -o \
jsonpath="{.items[?(@.metadata.annotations['kubernetes\.io/service-account\.name']=='default')].data.token}" \
|base64 --decode)

echo $token
