import http from 'k6/http';
import { check, sleep } from 'k6';

export let options = {
  stages: [
    { duration: '30s', target: 50 },
    { duration: '30s', target: 100 },
    { duration: '30s', target: 200 },
    { duration: '30s', target: 300 },
    { duration: '30s', target: 400 },
    { duration: '30s', target: 500 },
    { duration: '30s', target: 600 },
    { duration: '30s', target: 700 },
    { duration: '30s', target: 800 },
    { duration: '30s', target: 900 },
    { duration: '30s', target: 1000 },
    { duration: '1m', target: 50 },
    { duration: '1m', target: 5 }
  ],
};

// export let options = {
//   stages: [
//     { duration: '30s', target: 5 },
//     { duration: '30s', target: 10 },
//     { duration: '30s', target: 20 },
//     { duration: '30s', target: 30 },
//     { duration: '90s', target: 50 },
//     { duration: '30s', target: 60 },
//     { duration: '30s', target: 30 },
//     { duration: '30s', target: 0 }
//   ],
// };

export default function() {
  let res = http.get('http://localhost:32778/blocking/delayed/customers?delay=0.5f');
  check(res, { 'status was 200': r => r.status == 200 });
  sleep(1);
}
