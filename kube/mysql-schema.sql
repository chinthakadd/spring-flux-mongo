-- RUN THIS MANUALLY AFTER PORT FORWARD
-- k port-forward svc/mysql-release 3306:3306
create database if not exists `reactivetest`;
use `reactivetest`;

CREATE TABLE IF NOT EXISTS `customer` (
`CUSTOMER_NUMBER` int NOT NULL,
`NAME` varchar(45) DEFAULT NULL,
`CITY` varchar(45) DEFAULT NULL,
`STATE` varchar(45) DEFAULT NULL,
`POSTAL_CODE` varchar(45) DEFAULT NULL,
`COUNTRY` varchar(45) DEFAULT NULL,
`LAST_NAME` varchar(45) DEFAULT NULL,
`FIRST_NAME` varchar(45) DEFAULT NULL,
`PHONE` varchar(45) DEFAULT NULL,
`ADDRESS_1` varchar(45) DEFAULT NULL,
`ADDRESS_2` varchar(45) DEFAULT NULL,
PRIMARY KEY (`CUSTOMER_NUMBER`)
);


use `reactivetest`;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GET_CUSTOMERS_WITH_DELAY`(
IN SLEEP_TIME DECIMAL(3,2)
)
BEGIN
DO SLEEP(SLEEP_TIME);
SELECT
CUSTOMER_NUMBER,
NAME,
CITY,
STATE,
POSTAL_CODE,
COUNTRY,
LAST_NAME,
FIRST_NAME,
PHONE
FROM
customer
ORDER BY NAME;
END$$
DELIMITER ;


use `reactivetest`;
insert into customer values ("1","Atelier graphique","Nantes","","44000","France","Schmitt","Carine ","40.32.2555", NULL, NULL)
insert into customer values ("2","Australian Collectors, Co.","Melbourne","Victoria","3004","Australia","Ferguson","Peter","03 9520 4555",NULL,NULL);
insert into customer values ("3","La Rochelle Gifts","Nantes","","44000","France","Labrune","Janine ","40.67.8555",NULL,NULL);
insert into customer values ("4","Baane Mini Imports","Stavern","","4110","Norway","Bergulfsen","Jonas ","07-98 9555",NULL,NULL);
insert into customer values ("5","Signal Gift Stores","Las Vegas","NV","83030","USA","King","Jean","7025551838",NULL,NULL);