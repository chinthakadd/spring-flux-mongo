#!/usr/bin/env bash

kind load docker-image --name rx-kube dev.local/web-mysql:0.0.1
kind load docker-image --name rx-kube dev.local/flux-mysql:0.0.1

kubectl apply -f ./kube-flux-mysql.yaml
kubectl apply -f ./kube-web-mysql.yaml