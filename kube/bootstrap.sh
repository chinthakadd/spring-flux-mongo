#!/usr/bin/env bash

helm install prom-release stable/prometheus-operator

export POD_NAME=$(kubectl get pods --namespace default -l "app.kubernetes.io/name=grafana" -o jsonpath="{.items[0].metadata.name}")
# kubectl --namespace default port-forward $POD_NAME 3000

export PROM_POD_NAME=$(kubectl get pods --namespace default -l "prometheus=prometheus" -o jsonpath="{.items[0].metadata.name}")
# kubectl --namespace default port-forward $PROM_POD_NAME 9090

helm install mongo-release stable/mongodb --set values.service.type=NodePort \
--set values.service.nodePort=32780 --set values.usePassword=false --set volumePermissions.enabled=true \
--set mongodbUsername=minionuser,mongodbPassword=minionuser,mongodbDatabase=miniondb

export MONGO_POD_NAME=$(kubectl get pods --namespace default -l "app=mongodb" -o jsonpath="{.items[0].metadata.name}")
kubectl --namespace default port-forward $MONGO_POD_NAME 27017
# kubectl port-forward --namespace default svc/mongo-release-mongodb 27017:27017


helm install mysql-release -f ./sql-values.yaml stable/mysql

# GRafana Dashboard: https://grafana.com/grafana/dashboards/4701



#kubectl get secret --namespace default grafana-release -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
# USER: ADMIN
# PASSWORD: T5SqflwRfG1cWjozeAJ8NictokKMOh5DWTExK0oq

