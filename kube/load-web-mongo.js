import http from 'k6/http';
import { check, sleep } from 'k6';

export let options = {
  stages: [
    { duration: '30s', target: 5 },
    { duration: '30s', target: 10 },
    { duration: '30s', target: 20 },
    { duration: '30s', target: 30 },
    { duration: '30s', target: 40 },
    { duration: '30s', target: 50 },
    { duration: '30s', target: 60 },
    { duration: '30s', target: 70 },
    { duration: '30s', target: 80 },
    { duration: '60s', target: 90 },
    { duration: '60s', target: 100 },
    { duration: '30s', target: 50 },
    { duration: '30s', target: 0 }
  ],
};

export default function() {
  let res = http.get('http://localhost:32779/blocking/customers');
  check(res, { 'status was 200': r => r.status == 200 });
  sleep(1);
}
