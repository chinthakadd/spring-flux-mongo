#!/usr/bin/env bash

# KubeAdm Configuration Link: https://medium.com/@kosta709/kubernetes-by-kubeadm-config-yamls-94e2ee11244
kind create cluster --name rx-kube --config=./kind-config.yaml

kubectl apply -f ./kube-dashboard.yaml

# Cluster Role Binding
kubectl create clusterrolebinding default-admin --clusterrole cluster-admin --serviceaccount=default:default

# Get Token
token=$(kubectl get secrets -o \
jsonpath="{.items[?(@.metadata.annotations['kubernetes\.io/service-account\.name']=='default')].data.token}" \
|base64 --decode)

echo $token


