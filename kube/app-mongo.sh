#!/usr/bin/env bash

kind load docker-image --name rx-kube dev.local/sfm:0.0.1
kind load docker-image --name rx-kube dev.local/swm:0.0.1

kubectl apply -f ./kube-flux-mongo.yaml
kubectl apply -f ./kube-web-mongo.yaml