package com.virtusa.minion.demo.fluxmongo.springfluxmongo;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import static java.time.Duration.ofMillis;
import static java.time.Duration.ofMinutes;

//@SpringBootTest
class LoadTester {

    @Test
    void testNonBlocking() {
        Flux.range(1, 100000)
//                .delayElements(ofMillis(100))
                .flatMap(i -> {
                    System.out.println("Request " + i + " started...");
                    return WebClient.create("http://localhost:32780/non-blocking/customers")
                            .get()
                            .accept(MediaType.APPLICATION_STREAM_JSON)
                            .retrieve()
                            .bodyToFlux(Customer.class);
                })
                .blockLast(ofMinutes(15));
    }

    @Test
    void testBlocking() {
        Flux.range(1, 100000)
//                .delayElements(ofMillis(100))
                .flatMap(i -> {
                    System.out.println("Request " + i + " started...");
                    return WebClient.create("http://localhost:32779/blocking/customers")
                            .get()
                            .accept(MediaType.APPLICATION_JSON)
                            .retrieve()
                            .bodyToFlux(Customer.class);
                })
                .blockLast(ofMinutes(15));
    }
}
