package com.virtusa.minion.demo.fluxmongo.springfluxmongo;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface CustomerReactiveRepository extends ReactiveMongoRepository<Customer, String> {

    Mono<Customer> findByFirstName(String firstName);
}
