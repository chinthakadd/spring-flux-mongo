package com.virtusa.minion.demo.fluxmongo.springfluxmongo;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;
import java.time.temporal.TemporalUnit;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

import static org.springframework.http.MediaType.APPLICATION_STREAM_JSON_VALUE;

@RestController
public class CustomerReactiveController {

    private CustomerReactiveRepository customerReactiveRepository;

    public CustomerReactiveController(CustomerReactiveRepository customerReactiveRepository) {
        this.customerReactiveRepository = customerReactiveRepository;
    }

    @GetMapping(path = "/non-blocking/customers", produces = APPLICATION_STREAM_JSON_VALUE)
    public Flux<Customer> getNonBlockingCustomer() {
        return customerReactiveRepository.findAll();
    }

}
