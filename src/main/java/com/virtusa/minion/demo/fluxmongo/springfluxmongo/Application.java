package com.virtusa.minion.demo.fluxmongo.springfluxmongo;

import io.micrometer.prometheus.PrometheusMeterRegistry;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Example;
import org.springframework.data.mongodb.core.query.Query;
import reactor.core.publisher.Signal;

import java.util.function.Consumer;

import static org.springframework.data.mongodb.core.query.Criteria.where;

@SpringBootApplication
public class Application implements CommandLineRunner {

    @Autowired
    private CustomerReactiveRepository repository;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) {
        repository.deleteAll().block();
        for (int i = 0; i < 10; i++) {
            repository.save(new Customer("Alice-" + i, "Smith")).block();
        }
    }

}
